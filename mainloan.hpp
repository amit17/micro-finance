
#include <eosiolib/asset.hpp>
#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/transaction.hpp>
#include <eosiolib/crypto.h>
#include <eosiolib/time.hpp>
#include <eosiolib/system.h>
#include <string>
#include <vector>
#include <cmath>
// #include <eosio/eosio.hpp>
// #include <eosio/time.hpp>
// #include <eosio/system.hpp>

using namespace eosio;

using namespace std;
using eosio::time_point_sec;
// using eosio::current_time_point;

class[[eosio::contract]] mainloan : public eosio::contract
{
private:
  static inline time_point_sec current_time_point_sec()
  {
    return time_point_sec(now());
  }

  struct [[eosio::table]] borrower_info
  {
    name acc_name;
    string location;
    uint64_t b_phone;
    // uint64_t credit_score=0;

    auto primary_key() const
    {
      return acc_name.value;
    }
  };

  struct [[eosio::table]] underwriter_info
  {
    name acc_name;
    uint64_t interest_rate;
    uint64_t giving_loan_amunt;
    auto primary_key() const
    {
      return acc_name.value;
    }
  };

  struct [[eosio::table]] requestForLoan
  {
    uint64_t req_id;
    name borr_name;
    name uwr_name;
    uint64_t loan_amount;
    uint64_t interest_rate;
    uint64_t loan_duration;
    bool req_status = false;
    uint64_t primary_key() const
    {
      return req_id;
    }
    auto get_borrower() const
    {
      return borr_name.value;
    }
  };

  struct [[eosio::table]] payment_made
  {
    uint64_t pay_id;
    time_point_sec pay_date;

    uint64_t loan_id;
    uint64_t pay_amunt;
    name pay_from;

    uint64_t primary_key() const
    {
      return pay_id;
    }
  };

  struct [[eosio::table]] loan_info
  {
    uint64_t loan_id;
    name uwr_name;
    uint64_t principle_amount;
    uint64_t lending_amount;
    name borr_name;
    uint64_t interest_rate;        //annual
    uint64_t payment_duration = 1; //month   //total time ~ 1month ~ 30days
    uint64_t emi;
    uint64_t inc_loan;             //NOT NEEDED?
    uint64_t terms = 4;            //4 weeks per month => each installment
    time_point_sec last_pay_date;

    uint64_t primary_key() const
    {
      return loan_id;
    }
    auto get_uwr_name() const
    {
      return uwr_name.value;
    }
    
    auto get_borr_name() const
    {
      return borr_name.value;
    }
    
  };

  struct [[eosio::table]] interest_info{
    uint64_t interest_id;
    uint64_t loan_id;
    uint64_t interest;

      uint64_t primary_key() const
    {
      return interest_id;
    }

  };
  

  typedef eosio::multi_index<"borrower"_n, borrower_info> borrower;
  typedef eosio::multi_index<"underwriter"_n, underwriter_info> underwriter;
  typedef eosio::multi_index<"requestloan"_n, requestForLoan> requestloan;
  typedef eosio::multi_index<"loan"_n, loan_info>loan;
  typedef eosio::multi_index<"paymentmade"_n, payment_made> paymentmade;                  
  typedef eosio::multi_index<"interest"_n,interest_info>interest;
                    // indexed_by<"loanid"_n, const_mem_fun<emi_info, uint64_t, &emi_info::from_loan_id>>>interest;
                               

  borrower borr_table;
  underwriter uwr_table;
  requestloan reqloan_table;
  loan loan_table;
  paymentmade paymade_table;
  interest interest_table;

public:
  using contract::contract;

  mainloan(eosio::name receiver, eosio::name code, datastream<const char *> ds) : eosio::contract(receiver, code, ds),
                                                                                  borr_table(receiver, code.value),
                                                                                  uwr_table(receiver, code.value),
                                                                                  reqloan_table(receiver, code.value),
                                                                                  loan_table(receiver, code.value),
                                                                                  paymade_table(receiver, code.value),
                                                                                  interest_table(receiver,code.value){}
  

  [[eosio::action]] 
  void addborrower(name acc_name, string location,
                                     uint64_t b_phone);

  [[eosio::action]] 
  void adduwr(name acc_name,uint64_t interest_rate, uint64_t giving_loan_amunt);

  [[eosio::action]] 
  void reqloan(name borrower, name uwr_name, uint64_t loan_amnt, uint64_t interest_rate,
                                 uint64_t loan_duration);

  [[eosio::action]] 
  void approveloan(uint64_t req_id, name uwr);

  [[eosio::action]] 
  void addloan(name uwr_name, name borr_name, uint64_t loan_amnt,
                                 uint64_t rate, uint64_t payment_duration, uint64_t req_id);

  [[eosio::action]] 
  void payconfirm(uint64_t pay_amunt,name from, uint64_t loan_id);


  [[eosio::action]] 
  void calemi(uint64_t loan_id);

  [[eosio::action]] 
  void calinterest(uint64_t loan_id);

};


