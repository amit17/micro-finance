#include "/home/guru/contracts/mainloan/mainloan.hpp"
#include<stdio.h>

void mainloan::addborrower(name acc_name, string location,
                           uint64_t b_phone)
{
  print("Adding borrower: ", acc_name);
  // require_auth( _self ); //authorization of the contract account
  //eosio::check(loan_individual >= 0, "loan must be positive");

  borr_table.emplace(get_self(), [&](auto &b) {
    b.acc_name = acc_name;
    b.location = location;
    b.b_phone = b_phone;
  });
}

void mainloan::adduwr(name acc_name,uint64_t interest_rate,
                      uint64_t giving_loan_amunt)
{
  print("Adding underwriter", acc_name);
  // require_auth( _self );
  //eosio::check(value_score>=300 && value_score<=900, "credit score is always postive and ranges from 300-900");

  uwr_table.emplace(get_self(), [&](auto &u) {
    u.acc_name = acc_name;
    u.interest_rate = interest_rate;
    u.giving_loan_amunt = giving_loan_amunt;
  });
}

void mainloan::reqloan(name borr_name, name uwr_name, uint64_t loan_amnt, uint64_t interest_rate,
                       uint64_t loan_duration)
{
  // require_auth( _self );
  eosio::check(loan_amnt > 0, "Cannot loan in negatives!");
  eosio::check(interest_rate >= 0, "Interst rate cannot be negative!");
  //eosio::check(pay_time>0, "Cannot be zero or negative!");
  auto borrower = borr_table.find(borr_name.value);
  auto uwr = uwr_table.find(uwr_name.value);
  eosio::check(borrower != borr_table.end(), "Borrower doesn't exist.");

  reqloan_table.emplace(get_self(), [&](auto &r) {
    r.req_id = reqloan_table.available_primary_key();
    r.borr_name = borr_name;
    // r.borr_id = borrower->b_id;

    r.uwr_name = uwr_name;
    // r.uwr_id = uwr->acc_id;
    r.loan_amount = loan_amnt;
    r.interest_rate = interest_rate;
    r.loan_duration = loan_duration;
  });
}

void mainloan::approveloan(uint64_t req_id, name uwr)
{
  // require_auth( _self );
  auto reqloan_id = reqloan_table.find(req_id);
  auto uwrname = reqloan_id->uwr_name;
  // 
  if (uwr == uwrname && req_id == reqloan_id->req_id)
  {
    reqloan_table.modify(reqloan_id, _self, [&](auto &rt) {
      rt.req_status = true;
    });
  }
  else
    print("cant approve the loan due to wrong request id ");
}

void mainloan::addloan(name uwr_name, name borr_name, uint64_t loan_amnt, uint64_t rate,
                       uint64_t payment_duration, uint64_t req_id)
{
  // require_auth( _self );
  // eosio::print("request id: ",loan_req);
  // eosio::print("request status :  ",loan_req.req_status);
  auto loan_req = reqloan_table.find(req_id); 
   eosio::check(loan_req->req_status ==true, "Loan request has not been approved.Loan can not be added");
  if (loan_req->req_status == true)
  {
    eosio::check(loan_amnt > 0, "Cannot loan in negatives!");
    eosio::check(rate >= 0, "Interst rate cannot be negative!");
    auto borrower = borr_table.find(borr_name.value);
    auto uwr = uwr_table.find(uwr_name.value);

    eosio::check(borrower != borr_table.end(), "Borrower doesn't exist.");
    eosio::check(uwr != uwr_table.end(), "Lender doesn't exist.");

    loan_table.emplace(get_self(), [&](auto &l) {
      l.loan_id = loan_table.available_primary_key();
      l.uwr_name = uwr_name;
      l.principle_amount = loan_amnt;
      l.lending_amount = loan_amnt;
      l.borr_name = borr_name;
      l.interest_rate = rate;
      l.payment_duration = payment_duration;
      l.inc_loan = (l.lending_amount) / 4.0;
      l.terms = 4;
      
    });
    print("Loan Added");
  }
}

void mainloan::payconfirm(uint64_t pay_amunt,
                          name from, uint64_t loan_id)
{
  // require_auth(_self);
  auto loan = loan_table.find(loan_id);
  paymade_table.emplace(get_self(), [&](auto &p) {
    p.pay_id = paymade_table.available_primary_key();
    p.pay_date = current_time_point_sec();
    p.pay_amunt = pay_amunt;
    p.pay_from = from;
    p.loan_id = loan_id;
  });
  loan_table.modify(loan, _self, [&](auto &l) {
    l.last_pay_date = current_time_point_sec();
    l.lending_amount = loan->lending_amount - pay_amunt;
  });
}

void mainloan :: calinterest(uint64_t loan_id)
{
  auto loan =loan_table.find(loan_id);
  float emi=loan->emi;
  float rate =loan->interest_rate;
  auto tenure= loan->payment_duration;
  float lending_amount= loan->lending_amount;
    eosio::print("Interest Rate: ", rate);
      eosio::print("  Tenure: ", tenure);
        eosio::print("  Lending Amount : ", lending_amount);
  rate=rate/100;
  float calrate= rate/12;
    eosio::print("  calculated rate: ", calrate);    
  float interest = lending_amount * calrate ;
  eosio::print("  Interest calculated: ", interest);
    // eosio::print(" EMI ID: ", emi_id);
  lending_amount=lending_amount - (emi-interest);

    interest_table.emplace(get_self(), [&](auto &it) {
      it.interest_id = interest_table.available_primary_key();
      it.loan_id = loan->loan_id;
      it.interest= interest;
  });

    loan_table.modify(loan,_self, [&](auto &l) {
     l.lending_amount = lending_amount;
  });
}



void mainloan :: calemi(uint64_t loan_id)
{
  auto loan=loan_table.find(loan_id);
  eosio::check(loan->loan_id >=0, "loan id doesn't exsist!!");
  auto principal= loan->principle_amount;
  float rate = loan->interest_rate;
  auto tenure= loan->payment_duration;
  rate=rate/100;
  float calrate= rate/12;
  
  // EMI = [P x R x (1+R)^N]/[{(1+R)^N}-1]
  
  float a=1+calrate;
  float  po=std::pow(a,tenure);
  float numerator =(principal*calrate)*po;
  float denominator = po-1;

  float emi =numerator/denominator;
  eosio::print("  EMI per month: ", emi); 


    loan_table.modify(loan,_self, [&](auto &l) {
     l.emi = emi;
  });

}


